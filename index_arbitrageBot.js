const userConfig = require('./config/config');
const BitMEXClient = require('bitmex-realtime-api');
var apiKeyInfo = {
    TEST_apiKey: "XX7U6-UdV4dwaoYS5JPbo9Cl",
    TEST_apiSecret: "QHGVI0aqbIjzKNRtMAMDBn3CWcX3Jnz8alkv35A9jAtdgocS",
    _apiKey: "jCmVNPuOzKkJNc4Hs7rxA9T_", //guybitmex2
    _apiSecret: "8IYRB7numEXqT40tnHYd_pj6sDnrKdo8dvuz6RmBrhp7DM7I" //guybitmex2
    // _apiKey: "TOwmvc8yJbuft_hDjrl9jXp3", //guybitmex1
    // _apiSecret: "JJPC9jo3uliBPz-UjFzDm1Sx2yXBYpTV5DiUULa-Fd0Xfdru" //guybitmex1
};
if (userConfig.testnet) {
    _apiKey = apiKeyInfo.TEST_apiKey;
    _apiSecret = apiKeyInfo.TEST_apiSecret;
} else {
    _apiKey = apiKeyInfo._apiKey;
    _apiSecret = apiKeyInfo._apiSecret;
}

const client = new BitMEXClient({
    testnet: userConfig.testnet,
    apiKeyID: _apiKey,
    apiKeySecret: _apiSecret,
    maxTableLen: userConfig.maxTableLen  // the maximum number of table elements to keep in memory (FIFO queue)
});

client.on('error', console.error);

async function monitorOrderbook() {
    //await Timeout.set(2000);
    client.addStream('XBTUSD', 'orderBookL2_25', function (data) {
        //logger.info(data);

        //important: these two variables must be defined here so that they are re-evaluated everytime we go through the data...
        var highestBuy = 0;
        var lowestSell = 1000000;
        //loop over the array of objects and check what the highest sell price is
        data.forEach(function (orderBookItem) {
            if (orderBookItem.side === "Sell") {
                if (orderBookItem.price < lowestSell) {
                    lowestSell = orderBookItem.price;
                }
            }
            if (orderBookItem.side === "Buy") {
                if (orderBookItem.price > highestBuy) {
                    highestBuy = orderBookItem.price;
                }
            }
        });
    });
}

monitorOrderbook();
