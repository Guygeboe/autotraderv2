var makeRequest = require('./BitMexrequest').makeRequest;
const userConfig = require('./config/config');
const logger = require('./logger');
var index = require('./index');
var Timeout = require('await-timeout');
var CronJob = require('cron').CronJob;

//TODO: periodically update the orders.

// function runOneAmendOrderAtATimeCallback(props, callback){

// }

// /**
//  * Make sure that only one amendOrder is being handled at a time
//  */
var ordersAmendArray = [];

var runningLimitOrderHandle = false;

async function runOneAmendOrderAtATime(props, callback) {
    var count = 0;
    if (runningLimitOrderHandle) {
        setTimeout(runOneAmendOrderAtATime, 100, props, callback);
    } else {
        runningLimitOrderHandle = true;
        //check if the order was already amended once
        //COMMENTED OUT FOR NOW...
        // var orderAlreadyAmendedOnce = false;
        // for (let index = 0; index < ordersAmendArray.length; index++) {
        //   const element = ordersAmendArray[index];
        //   if (element.orderID == props.orderID){
        //     orderAlreadyAmendedOnce = true;
        //     ordersAmendArray[index].count = ordersAmendArray[index].count + 1;
        //     count = ordersAmendArray[index].count;
        //   }
        // }
        // if (!orderAlreadyAmendedOnce){
        //   ordersAmendArray.push({orderID : props.orderID, count: 1})
        //   count = 1;
        // }

        // if (count > 5 ){
        //   //considering marketing out because we've amended the order now for 5 times...
        //   logger.info("considering marketing out because we've amended the order now for 5 times...");
        // }
        // else {
        //  //what could have happened in the meantime?
        //  //1) the order could have been filled already...
        //  //2) things to do: check position, cancel orders, check if position is the same, market out.
        //  //3) if position is not the same, what happened???
        // }

        logger.info("amending order price to " + JSON.stringify(props));
        const amendOrderResult = await amendOrder(props);
        //TODO: keep a list of orders and update a specific order with its status in the future...
        var orderStuff = [];
        orderStuff.push(amendOrderResult);
        index.orderBookData.setOrders(orderStuff);

        //log(logSymbols.info, 'Listening for new TradingView notifications...');
        //console.log("debug: setting order handle to false");
        runningLimitOrderHandle = false;
        callback(amendOrderResult);
    }
}


async function getOrders() {
    logger.info("starting getOrders CRON");
    try {
        new CronJob('*/6 * * * * *', async function () {
            getOrdersByPOST();
        }, null, true, 'America/Los_Angeles', false, false);
    } catch (e) {
        logger.info("error, see error log");
        logger.error(e);
        //TODO: email yourself that an error occured!!!
    }
    ;
}

async function monitorOrders() {
    try {
        new CronJob('*/1 * * * * *', async function () {
            monitorOrderAndAmend();
        }, null, true, 'America/Los_Angeles', false, false);
    } catch (e) {
        logger.info("error, see error log");
        logger.error(e);
        //TODO: email yourself that an error occured!!!
    }
    ;
}

async function getOrdersByPOST() {
    try {
        orders = await makeRequest('GET', 'order', {
            filter: {symbol: userConfig.symbol, "open": true}
        });
        index.orderBookData.setOrders(orders);
        return orders;
    } catch (e) {
        //getLimitOrderFilled(orderResult, true);
        console.log(e);
    }
}

function monitorOrderAndAmend() {
    //get order status...
    let orders;
    orders = index.orderBookData.getOrders();

    //logger.info("found " + orders.length + " orders");
    if (typeof orders !== 'undefined') {
        for (const order of orders) {
            //if (order.orderID === orderResult.orderID) {
            //we're skipping StopLimit orders because they should not be adjusted...
            if (order.ordStatus != "Filled" && order.ordType == "Limit" && order.symbol === userConfig.symbol) {
                //unfilled order found, this takes precedence..
                index.orderBookData.setLimitOrderInProcess(true);

                //get latest bid and see if it is still valid...
                if (order.side === "Sell") {
                    let bestAsk = index.orderBookData.getAsk();
                    if (order.price === bestAsk) {  //keep monitoring it?
                        //logger.info("price is still fine, keep monitoring")
                        //getLimitOrderFilled(orderResult, true);
                    } else {
                        //amend order with new price and monitor again?
                        //logger.info("amending order price to " + bestAsk);
                        //const amendOrderResult = await makeRequest('PUT', 'order', { price: bestAsk, orderID: order.orderID });

                        //, text : order.text seems to be amended by Bitmex themselves...
                        runOneAmendOrderAtATime({price: bestAsk, orderID: order.orderID}, function (amendOrderResult) {
                            //logger.info("amend complete?")
                        })
                        //getLimitOrderFilled(amendOrderResult, true);
                    }
                }
                if (order.side === "Buy") {
                    let bestBid = index.orderBookData.getBid();
                    if (order.price === bestBid) {
                        //keep monitoring it?
                        //logger.info("price is still fine, keep monitoring")
                        //getLimitOrderFilled(orderResult, true);
                    } else {
                        //amend order with new price and monitor again?
                        //logger.info("amending order price to " + bestBid);
                        //const amendOrderResult = await makeRequest('PUT', 'order', { price: bestBid, orderID: order.orderID });
                        runOneAmendOrderAtATime({price: bestBid, orderID: order.orderID}, function (amendOrderResult) {
                            //logger.info("amend complete?")
                        })
                        //getLimitOrderFilled(amendOrderResult, true);
                    }
                } else if (order.ordStatus == "Filled") {
                    logger.info("order is filled, hoorah");
                }
            } else {
                index.orderBookData.setLimitOrderInProcess(false);
                //logger.info("order status is " + order.ordStatus);
            }
            //}
        }
    }
}

//('PUT', 'order', { price: bestBid, orderID: order.orderID });
async function amendOrder(props) {
    const amendOrderResult = await makeRequest('PUT', 'order', props);
    console.log("debug: returning amendOrder");
    return amendOrderResult;
}

//TODO: write an init function
// monitorOrders();
// getOrders();

getLimitOrderFilled = async function (orderResult, continued) {
    //return 1;
    //this function is WAS temporarily disabled because of the functions in this file .
    // // monitorOrders();
    // // getOrders();
    // but these are disabled now as we don't want ALL limit orders to get filled, they should just 'stay'.
    if (continued) {
        // logger.info("continuing to monitor order...");
    } else {
        // logger.info("starting to monitor order...");
    }
    //wait four seconds...and check if the price still matches the bestBid or Ask
    await Timeout.set(10000);
    //get order status...
    let orders;
    try {
        orders = await makeRequest('GET', 'order', {
            filter: {symbol: userConfig.symbol, "open": true}
        });
    } catch (e) {
        getLimitOrderFilled(orderResult, true);
    }

    // logger.info("found " + orders.length + " orders");
    for (const order of orders) {
        if (order.orderID === orderResult.orderID) {
            //we're skipping StopLimit orders because they should not be adjusted...
            if (order.ordStatus != "Filled" && order.ordType == "Limit") {
                //get latest bid and see if it is still valid...
                if (order.side === "Sell") {
                    let bestAsk = index.orderBookData.getAsk();
                    if (order.price === bestAsk) {  //keep monitoring it?
                        // logger.info("price is still fine, keep monitoring");
                        getLimitOrderFilled(orderResult, true);
                    } else {
                        //amend order with new price and monitor again?
                        logger.info("amending order price to " + bestAsk);
                        const amendOrderResult = await makeRequest('PUT', 'order', {
                            price: bestAsk,
                            orderID: order.orderID
                        });
                        getLimitOrderFilled(amendOrderResult, true);
                    }
                }
                if (order.side === "Buy") {
                    let bestBid = index.orderBookData.getBid();
                    if (order.price === bestBid) {
                        //keep monitoring it?
                        // logger.info("price is still fine, keep monitoring");
                        getLimitOrderFilled(orderResult, true);
                    } else {
                        //amend order with new price and monitor again?
                        logger.info("amending order price to " + bestBid);
                        const amendOrderResult = await makeRequest('PUT', 'order', {
                            price: bestBid,
                            orderID: order.orderID
                        });
                        getLimitOrderFilled(amendOrderResult, true);
                    }
                } else if (order.ordStatus == "Filled") {
                    logger.info("order is filled, hoorah");
                }
            } else {
                logger.info("order status is " + order.ordStatus);
            }
        }
    }
}

async function postOrderCallback(side, dividePositionBy, orderType, price, callback) {
    var result = await postOrder(side, dividePositionBy, orderType, price, "");
    callback(result);
}

//for dividePositionBy, valid values are 1 for selling 100% or for example .8 for selling 80%
postOrderSimple = async function (side, orderType, price, annotationText, positionSize) {
    orderResult = await makeRequest('POST', 'order', {
        symbol: userConfig.symbol, orderQty: positionSize, ordType: orderType, price: price, side: side, text: annotationText, execInst: 'ParticipateDoNotInitiate'
    });

    if (typeof orderResult.error !== 'undefined') {
        //error occured, go flat?
        logger.error("going flat for safety reasons . " + JSON.stringify(orderResult.error));
        // closePosition("going flat by closing position because of error in app from postOrder");
        return 1;
    } else {
        //order succesfully placed?
        // logger.info("order placed?" + orderResult.ordStatus + ' side: ' + orderResult.side + ' symbol: ' + orderResult.symbol);
        return orderResult;
    }
};

cancelOrder = async function (params) {
    orderResult = await makeRequest('DELETE', 'order', params, 'DISABLEDx-www-form-urlencoded');

    // console.log(orderResult);
    return orderResult;
};

cancelOrders = async function (params) {
    if (!Array.isArray(params)) {
        throw 'params is not an array';
    }
    let requestBody = { orderID : params};
    orderResult = await makeRequest('DELETE', 'order', requestBody, 'DISABLEDx-www-form-urlencoded');

    // console.log(orderResult);
    return orderResult;
};

cancelAllOrders = async function (symbol) {
    orderResult = await makeRequest('DELETE', 'order/all', {symbol: symbol}, );

    // console.log(orderResult);
    return orderResult;
};


postBulkOrderSimple = async function (orders) {
    orderResult = await makeRequest('POST', 'order/bulk', { orders: orders});

    console.log("x-ratelimit-remaining: " +parseInt(orderResult.headers.get('x-ratelimit-remaining')));

    if (typeof orderResult.error !== 'undefined') {
        //error occured, go flat?
        logger.error("going flat for safety reasons . " + JSON.stringify(orderResult.error));
        // closePosition("going flat by closing position because of error in app from postOrder");
        return 1;
    } else {
        //order succesfully placed?
        logger.info("order placed?" + orderResult.ordStatus + ' side: ' + orderResult.side + ' symbol: ' + orderResult.symbol);
        return 0;
    }
}

//for dividePositionBy, valid values are 1 for selling 100% or for example .8 for selling 80%
postOrder = async function (side, dividePositionBy, orderType, price, annotationText) {
    //const orderResult = await ExecuteOrder();
    //now validate if the order is processed...
    if (typeof dividePositionBy === 'undefined') {
        dividePositionBy = 1;
    }
    if (typeof orderType === 'undefined') {
        orderType = "Market";
    }
    var currentPositionSide = "FLAT";
    var newPositionSize = 0;
    const minPositionSize = index.getMinimumPositionSize();
    var orderSide = "";
    try {
        const positionResult = await makeRequest('GET', 'position', {
            filter: {symbol: userConfig.symbol},
            columns: ['currentQty', 'avgEntryPrice'],
        });
        if (typeof positionResult[0] !== 'undefined') {
            var currentPositionSize = positionResult[0].currentQty;
        } else {
            var currentPositionSize = 0
        }

        if (currentPositionSize > 0) {
            //we are currently long!
            logger.info("current position is LONG");
            currentPositionSide = "BUY";
        } else if (currentPositionSize < 0) {
            logger.info("current position is SHORT");
            currentPositionSide = "SELL";
        } else if (currentPositionSize == 0) {
            currentPositionSide = "FLAT";
        } else {
            logger.error("something gone wrong with determining current position, " + currentPositionSize);
        }

        if (currentPositionSide == "FLAT" && side == "Flat") {
            //open an order with the configured size.
            logger.info("FLAT FLAT, staying flat...");
        } else {
            //determine what the position size needs to be and then determine how much you need to buy or sell...

            if (currentPositionSide == "FLAT") {
                //open an order with the configured size.
                newPositionSize = minPositionSize;
                orderSide = side;
            } else if (currentPositionSide == "BUY" && side == "Sell") {
                //newPositionSize = userConfig.positionSize * -2;
                //newPositionSize = ((userConfig.positionSize) - (userConfig.positionSize-currentPositionSize));
                newPositionSize = (currentPositionSize * -1) - minPositionSize;
                orderSide = "Sell";
                logger.info("Current position is buy and sending sell order.")
            } else if (currentPositionSide == "SELL" && side == "Buy") {
                newPositionSize = minPositionSize - currentPositionSize;
                orderSide = "Buy";
                logger.info("Current position is sell and sending buy order.")
            } else if (currentPositionSide == "BUY" && side == "Flat") {
                //newPositionSize = (userConfig.positionSize * -1) * dividePositionBy;
                newPositionSize = (currentPositionSize * -1) * dividePositionBy
                orderSide = "Sell";
                logger.info("Current position is buy and sending sell order to go (partially) flat.")
            } else if (currentPositionSide == "SELL" && side == "Flat") {
                //newPositionSize = userConfig.positionSize * 1 * dividePositionBy;
                newPositionSize = (currentPositionSize * -1) * dividePositionBy
                orderSide = "Buy";
                logger.info("Current position is sell and sending buy order to go (partially) flat.")
            } else {
                //no bueno? Keep current position or perhaps close...
            }

            //to prevent incorrect order quantity error, round down (or up):
            if (newPositionSize < 0) {
                newPositionSize = Math.ceil(newPositionSize);
            } else {
                newPositionSize = Math.floor(newPositionSize);
            }
            // override...DCA is 8 times the position size so hence increasing it here...
            newPositionSize = newPositionSize * 1;

            //DEBUG EXTRA INFO
            logger.info("EXTRA DEBUG INFO: newPositionSize: " + newPositionSize + "  " + currentPositionSide + " " + side + " " + dividePositionBy + " " + currentPositionSize)

            if (newPositionSize == 0) {
                //cant sell a sell or buy a buy, not placing an order
                logger.info("not placing an order because size is 0")
            } else {
                logger.info("sending " + orderSide + " order");
                //TODO: create a limit order based on the ask/bid price?
                //considerations: check the price for bid/ask and then create a limit order. if the order is filled then done
                //if the order is partially filled then create a new one based on the remaining amount
                //if the calls fail then handle that...
                //if the price moves then update the limit price...don't create a new order.
                //https://gekko.wizb.it/docs/gekko-broker/sticky_order.html

                var orderResult = "";
                if (orderType === "Limit" || orderType === "LimitIfTouched") {
                    //determine prices
                    var limitOrderPrice = 0;
                    if (orderSide === "Sell") {
                        limitOrderPrice = index.orderBookData.getAsk();
                    } else {
                        limitOrderPrice = index.orderBookData.getBid();
                    }
                    if (limitOrderPrice == 0 || limitOrderPrice < 1) {
                        //this should not happen...but usually happens when there is an issue with init function so the prices were not retrieved.
                        logger.info("EXTRA DEBUG...realy have to check why this is happening...")
                        var price = await getPrice('XBT') //await makeRequest('GET', '/trade?symbol=XBT&count=1&reverse=true');
                        if (price > 0) {
                            limitOrderPrice = price;
                        } else {
                            //something went wrong!!
                            logger.info("something went wrong..." + price)
                        }
                    }
                    logger.info("EXTRA DEBUG..." + limitOrderPrice);
                    if (orderType === "LimitIfTouched") {
                        limitOrderPrice = price
                        logger.info("sending limit(IfTouched) order with price " + limitOrderPrice + " side:" + orderSide);
                        orderResult = await makeRequest('POST', 'order', {
                            symbol: userConfig.symbol,
                            orderQty: newPositionSize,
                            ordType: orderType,
                            stopPx: limitOrderPrice,
                            price: limitOrderPrice,
                            side: orderSide,
                            execInst: "LastPrice",
                            text: annotationText
                        });
                    } else {
                        logger.info("sending limit order with price " + limitOrderPrice + " side:" + orderSide);
                        orderResult = await makeRequest('POST', 'order', {
                            symbol: userConfig.symbol,
                            orderQty: newPositionSize,
                            ordType: orderType,
                            price: limitOrderPrice,
                            side: orderSide,
                            text: annotationText,
                            execInst: 'ParticipateDoNotInitiate'
                        });
                    }


                } else {
                    //market order
                    orderResult = await makeRequest('POST', 'order', {
                        symbol: userConfig.symbol,
                        orderQty: newPositionSize,
                        ordType: orderType,
                        side: orderSide,
                        text: annotationText
                    });

                }


                if (typeof orderResult.error !== 'undefined') {
                    //error occured, go flat?

                    logger.error("going flat for safety reasons . " + JSON.stringify(orderResult.error));
                    closePosition("going flat by closing position because of error in app from postOrder");
                    return 0;
                } else {
                    //order succesfully placed?
                    logger.info("order placed?" + orderResult.ordStatus + ' side: ' + orderResult.side + ' symbol: ' + orderResult.symbol);

                }
                var orderId = orderResult.orderID;
                if (userConfig.verifyPostedOrder) {
                    const getOrderResult = await makeRequest('GET', 'order');
                    //logger.info("Orders:")
                    //loop over orders and check if the orderId is there in the array
                    var foundOrder = false;
                    for (let i = 0; i < getOrderResult.length; i++) {

                        if (getOrderResult[i].orderID === orderId) {
                            //found it
                            //logger.info(JSON.stringify(getOrderResult[i]));
                            foundOrder = true;

                            if (getOrderResult[i].ordStatus != "Filled") {
                                logger.error("Order is not filled!")
                                logger.error("reason:" + getOrderResult[i].ordRejReason.toString())
                                logger.error("going flat for safety reasons [is disabled] ")
                                //order.closePosition("going flat by closing position because of error in app");
                            } else {
                                logger.info("Order is filled: " + getOrderResult[i].ordStatus);
                            }
                        }
                    }
                    if (!foundOrder) {
                        //TODO: probably want to email this as well??
                    }
                }
            }
            //return succesful processing
            return orderResult;
        }
    } catch (e) {
        logger.error(e);
        return 0;
        //TODO: email yourself that an error occured!!!
    }
    ;
}

//https://github.com/BitMEX/api-connectors/blob/master/auto-generated/javascript/docs/OrderApi.md#orderNew
//DEPRECATED
const ExecuteOrder = async function () {
    var verb = 'POST',
        path = '/api/v1/order',
        expires = Math.round(new Date().getTime() / 1000) + 60, // 1 min in the future
        //data = {symbol:"XBTUSD",orderQty:1,price:590,ordType:"Limit"};
        data = {symbol: userConfig.symbol, orderQty: 1, ordType: "Market"};

    // Pre-compute the postBody so we can be sure that we're using *exactly* the same body in the request
    // and in the signature. If you don't do this, you might get differently-sorted keys and blow the signature.
    var postBody = JSON.stringify(data);

    var signature = crypto.createHmac('sha256', _apiSecret).update(verb + path + expires + postBody).digest('hex');

    var headers = {
        'content-type': 'application/json',
        'Accept': 'application/json',
        'X-Requested-With': 'XMLHttpRequest',
        // This example uses the 'expires' scheme. You can also use the 'nonce' scheme. See
        // https://www.bitmex.com/app/apiKeysUsage for more details.
        'api-expires': expires,
        'api-key': _apiKey,
        'api-signature': signature
    };

    const requestOptions = {
        headers: headers,
        url: _url + path,
        method: verb,
        body: postBody
    };

    request(requestOptions, function (error, response, body) {
        if (error) {
            logger.info(error);
            return error;
        } else {
            logger.info(body);
            return body;
        }
    });
}

async function getPrice(symbol) {
    try {
        var quoteResult = await makeRequest('GET', '/trade?symbol=' + symbol + '&count=1&reverse=true');
        return quoteResult[0].price;
        //var price = quoteResult[0].price;
    } catch (e) {
        console.error(e);
        return 0;
        //TODO: email yourself that an error occured!!!
    }
    ;

}

async function closePosition(annotationText) {
    //this is deprecated
    //let response = await makeRequest('POST', '/order/closePosition');
    orderResult = await makeRequest('POST', 'order', {
        symbol: userConfig.symbol, execInst: "Close", text: annotationText
    });
    return orderResult;
}

async function getCandles(binSize) {
    // TODO: validate binSize...
    var query = {'binSize': binSize, 'symbol': userConfig.symbol, 'count': 100, reverse: true, partial: false}
    return one_m_candlesBucketed = await makeRequest('GET', '/trade/bucketed', query);
}

module.exports = {
    postOrder: postOrder,
    postOrderSimple: postOrderSimple,
    postBulkOrderSimple: postBulkOrderSimple,
    closePosition: closePosition,
    postOrderCallback: postOrderCallback,
    getCandles: getCandles,
    getPrice: getPrice,
    cancelOrder: cancelOrder,
    cancelOrders: cancelOrders,
    cancelAllOrders : cancelAllOrders,
    getLimitOrderFilled: getLimitOrderFilled,
    monitorOrderAndAmend: monitorOrderAndAmend,
    getOrdersByPOST: getOrdersByPOST
    //,runOneLimitOrderAtATime: runOneLimitOrderAtATime
}
