'use strict';

/**
 * @module tests/polling
 */
const assert = require('assert');
process.env.NODE_ENV = 'test';
const Store = require('data-store');
const store = new Store({path:process.cwd() + '\\config.json'});
store.set('stopTrading', 'true');
const index = require('../index');
var order = require('../order');

describe('testing trading', function () {
    it('can cancel all the orders', async function (done) {
        //this assumes a positionSize of 18
        const orderResult = await order.cancelAllOrders('XBTUSD');
        console.log(orderResult);
    });
    it('calculated the correct DCA level 1', function (done) {
        //this assumes a positionSize of 18
        const calculatedDCALevel = index.determineCurrentDCALevel(50);
        assert.strictEqual(calculatedDCALevel, 1);
        done();
    });
    it('calculated the correct DCA level 2', function (done) {
        //this assumes a positionSize of 18
        const calculatedDCALevel = index.determineCurrentDCALevel(126);
        assert.strictEqual(calculatedDCALevel, 2);
        done();
    });
    it('calculated the correct DCA level 3', function (done) {
        //this assumes a positionSize of 18
        const calculatedDCALevel = index.determineCurrentDCALevel(250);
        assert.strictEqual(calculatedDCALevel, 3);
        done();
    });
    it('calculated the correct DCA level 4', function (done) {
        //this assumes a positionSize of 18
        const calculatedDCALevel = index.determineCurrentDCALevel(550);
        assert.strictEqual(calculatedDCALevel, 4);
        done();
    });
    it('calculated the correct DCA level 5', function (done) {
        //this assumes a positionSize of 18
        const calculatedDCALevel = index.determineCurrentDCALevel(1140);
        assert.strictEqual(calculatedDCALevel, 5);
        done();
    });
    it('calculated the correct DCA level 6', function (done) {
        //this assumes a positionSize of 18
        const calculatedDCALevel = index.determineCurrentDCALevel(1200);
        assert.strictEqual(calculatedDCALevel, 6);
        done();
    });
    it('determines correct size per level 1', function (done) {
        //this assumes a positionSize of 18
        const calculatedDCALevel = index.determineDCASizePerLevel(18);
        assert.strictEqual(calculatedDCALevel[0], 36);
        done();
    });
    it('determines correct size per level 2', function (done) {
        //this assumes a positionSize of 18
        const calculatedDCALevel = index.determineDCASizePerLevel(18);
        assert.strictEqual(calculatedDCALevel[1], 72);
        done();
    });
});
