const _ = require('lodash'),
    log = require('fancy-log'),
    logSymbols = require('log-symbols'),
    chalk = require("chalk");
const userConfig = require('./config/config');
const symbol = userConfig.symbol;
const BitMEXClient = require('bitmex-realtime-api');
var Timeout = require('await-timeout');
const logger = require('./logger');
var papertradePrice = 0;
var hrstart = process.hrtime();
const moment = require('moment-timezone');
var sellSignals = 0;
let ROEmax = 0;
var orderBook = {bestBid: 0, bestAsk: 0};
//var me = require('./index');
var CronJob = require('cron').CronJob;
var tulind = require('tulind');
const {URLSearchParams} = require('url');
var nodemailer = require('nodemailer');

const telegramBot = require('./telegramBot');

let lastKnownAlertPosition = 0;
var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'guybitmex3@gmail.com',
        pass: 'ZeRgb6LNbbnTYuS'
    }
});
var BitMexApi = require('bit_mex_api');
var defaultClient = BitMexApi.ApiClient.instance;
let originalavgEntryPrice = 0;
let bias = "Buy";

var apiKeyInfo = {
    TEST_apiKey: "XX7U6-UdV4dwaoYS5JPbo9Cl",
    TEST_apiSecret: "QHGVI0aqbIjzKNRtMAMDBn3CWcX3Jnz8alkv35A9jAtdgocS",
    _apiKey: "jCmVNPuOzKkJNc4Hs7rxA9T_", //guybitmex2
    _apiSecret: "8IYRB7numEXqT40tnHYd_pj6sDnrKdo8dvuz6RmBrhp7DM7I" //guybitmex2
    // _apiKey: "TOwmvc8yJbuft_hDjrl9jXp3", //guybitmex1
    // _apiSecret: "JJPC9jo3uliBPz-UjFzDm1Sx2yXBYpTV5DiUULa-Fd0Xfdru" //guybitmex1
};

if (userConfig.testnet) {
    _apiKey = apiKeyInfo.TEST_apiKey;
    _apiSecret = apiKeyInfo.TEST_apiSecret;
} else {
    _apiKey = apiKeyInfo._apiKey;
    _apiSecret = apiKeyInfo._apiSecret;
}

const client = new BitMEXClient({
    testnet: userConfig.testnet,
    apiKeyID: _apiKey,
    apiKeySecret: _apiSecret,
    maxTableLen: userConfig.maxTableLen  // the maximum number of table elements to keep in memory (FIFO queue)
});
//handle errors here. If no 'error' callback is attached. errors will crash the client.
client.on('error', console.error);

var botInfo = {};
var stoplossTriggered = false;
const Store = require('data-store');
const store = new Store({path: 'config.json'});
var tradingHalted = false;
const mailOptions = {
    from: 'guybitmex3@gmail.com', // sender address
    to: 'guyhagemans@gmail.com', // list of receivers
    subject: 'issue with trading algo', // Subject line
    html: "<p>There's an issue</p>"// plain text body
};
var setTradingHalted = function (tradingHaltedBool) {
    tradingHalted = tradingHaltedBool;
};
var orderBookData = {
    setBid: function (newVal) {
        orderBook.bestBid = newVal;
    },
    setAsk: function (newVal) {
        orderBook.bestAsk = newVal;
    },
    getBid: function () {
        return orderBook.bestBid;
    },
    getAsk: function () {
        return orderBook.bestAsk;
    },
    setLimitOrderInProcess: function (newVal) {
        orderBook.limitOrderInProcess = newVal;
    },
    getLimitOrderInProcess: function () {
        return orderBook.limitOrderInProcess;
    },
    setOrders: function (newVal) {
        orderBook.orders = newVal;
    },
    getOrders: function () {
        return orderBook.orders;
    },
    setavgEntryPrice: function (newVal) {
        orderBook.avgEntryPrice = newVal;
    },
    getavgEntryPrice: function () {
        return orderBook.avgEntryPrice;
    },
    setunrealisedRoePcnt: function (newVal) {
        orderBook.unrealisedRoePcnt = newVal;
    },
    getunrealisedRoePcnt: function () {
        return orderBook.unrealisedRoePcnt;
    },
    setEMA: function (newVal) {
        orderBook.EMA = newVal;
    },
    getEMA: function () {
        return orderBook.EMA;
    },
    setpositionSize: function (newVal) {
        orderBook.positionSize = newVal;
    },
    getpositionSize: function () {
        return orderBook.positionSize;
    },
    setorderCooldownActive: function (newVal) {
        orderBook.orderCooldownActive = newVal;
    },
    getorderCooldownActive: function () {
        return orderBook.orderCooldownActive;
    }
};
var maxPrice = 0;
var trailingStopReset = true;

module.exports = {
    orderBookData: orderBookData,
    setTradingHalted: setTradingHalted,
    determineCurrentDCALevel: determineCurrentDCALevel,
    determineDCASizePerLevel: determineDCASizePerLevel,
    determineBuyOrSellBias: determineBuyOrSellBias,
    determineDCALevels: determineDCALevels,
    getMinimumPositionSize: getMinimumPositionSize,
    botInfo: botInfo,
    apiKeyInfo: apiKeyInfo
}

var order = require('./order');
var bitmexRequest = require('./BitMexrequest');
//disabled for now.
if (userConfig.websocketServer) {
    var server = require('./server');
}

logger.info("STARTING BOT, HI!");

async function monitorOrderbook() {
    client.addStream(symbol, 'orderBookL2_25', function (data) {
        //important: these two variables must be defined here so that they are re-evaluated everytime we go through the data...
        var highestBuy = 0;
        var lowestSell = 1000000;
        //loop over the array of objects and check what the highest sell price is
        data.forEach(function (orderBookItem) {
            if (orderBookItem.side === "Sell") {
                if (orderBookItem.price < lowestSell) {
                    lowestSell = orderBookItem.price;
                }
            }
            if (orderBookItem.side === "Buy") {
                if (orderBookItem.price > highestBuy) {
                    highestBuy = orderBookItem.price;
                }
                if (orderBookItem.price > 17350) {
                    // if (store.get('stopTrading') !== 'true') {
                    //     console.log("setting stopTrading in store to true");
                    //     store.set('stopTrading', 'true');
                    // }
                }
            }
        });

        orderBookData.setAsk(lowestSell);
        orderBookData.setBid(highestBuy);
    });
}

//stream for getting updates on the position.
async function monitorPosition() {
    //5s pause is required so that it can first get a good position info before it starts to sell.

    client.addStream(symbol, 'position', function (data) {

        if (typeof data[0] === 'undefined') {
            logger.error("cannot read position, leverage is set incorrectly to cross? Set it to 1x");
            orderBookData.setpositionSize(0);
        } else if (data[0].currentQty != 0) {
            orderBookData.setavgEntryPrice(data[0].avgEntryPrice);
            orderBookData.setunrealisedRoePcnt(data[0].unrealisedRoePcnt);
            orderBookData.setpositionSize(data[0].currentQty);
            store.set('currentState', "InPOSITION");
            if (data[0].currentQty > 0) {
                if (store.get('currentPosition') != "LONG") {
                    store.set('currentPosition', 'LONG');
                    logger.info("position monitor setting currentPosition to LONG");

                }
            } else if (data[0].currentQty < 0) {
                if (store.get('currentPosition') != "SHORT") {
                    store.set('currentPosition', 'SHORT');
                    logger.info("position monitor setting currentPosition to SHORT");

                }
            }
        } else if (data[0].currentQty == 0 && store.get('currentState') != "BUYING" && !stoplossTriggered) {
            //position should probably (need to test) be updated to 'FLAT' so that we can re-short or re-long
            logger.info("position monitor setting currentPosition to FLAT");
            store.set('currentPosition', 'FLAT');
            // temporarily disabled this because it might be causing issues...
            orderBookData.setpositionSize(data[0].currentQty);
        }
        if (data[0].currentQty) {
            if (data[0].currentQty && data[0].currentQty < 0) {
                telegramBot.sendAlert('Current position is negative!');
            }
            if (data[0].currentQty && data[0].currentQty > 12000) {
                if (lastKnownAlertPosition !== data[0].currentQty) {
                    lastKnownAlertPosition = data[0].currentQty;
                    telegramBot.sendAlert('Current position is large: ' + data[0].currentQty);
                }
            }
        }
        let percGain = 0;
        //TODO: do something with when there is no data[0].unrealisedPnlPcnt because sometimes the websocket breaks?
        if (!userConfig.paperTrading) {
            percGain = (data[0].unrealisedPnlPcnt) * 100;
            botInfo.percGain = percGain;
        } else {
            //check papertrade price is handled by stream for price.
        }

        //implementing a stoploss
        if (!stoplossTriggered && userConfig.stoplossEnabled) {
            if (percGain < userConfig.stoplossPercentage || (sellSignals > 0 && percGain < userConfig.stoplossPercentageAfterSellSignal)) {
                sellSignals++;
                logger.info("STOP LOSS. perc gain: " + percGain + " and price is " + data[0].lastPrice + " SellSignalvalue: " + sellSignals)
                stoplossTriggered = true;
                botInfo.percGain = percGain;
                botInfo.sellSignals = sellSignals;
                bitmexRequest.makeRequestCallback('DELETE', 'order/all', {}, function (result) {
                    order.postOrder("Flat", 1, "Limit", 0, "stoploss");
                });
            }
        }
    });
}

//this is a test..
DCAOrdersCreated = false;

async function initTrading() {
    //to be removed but for testing:
    logger.info("init CRON");

    logger.info("going to trade in 5s");
    //before anything else, determine the bias...
    determineTradingParameters();
    Timeout.set(5000);
    //get amount of DCAs
    try {
        new CronJob('*/15 * * * * *', async function () {
            runDCACheck();
        }, null, true, 'America/Los_Angeles', false, false);
        new CronJob('*/1 * * * * *', async function () {
            runBuyPosition();
        }, null, true, 'America/Los_Angeles', false, false);
        new CronJob('*/30 * * * * *', async function () {
            runStrategyMonitor();
        }, null, true, 'America/Los_Angeles', false, false);
        new CronJob('0 */2 * * * *', async function () {
            determineTradingParameters();
        }, null, true, 'America/Los_Angeles', false, false);
        new CronJob('*/1 * * * * *', async function () {
            // readEmailForAlerts();
        }, null, true, 'America/Los_Angeles', false, false);
    } catch (e) {
        logger.info("error, see error log");
        logger.error(e);
        //TODO: email yourself that an error occured!!!
    }
}

let lastKnownavgEntryPrice, DCAlastKnownavgEntryPrice = 0;
let DCAParameters = "";
let lastDCATime = null; // i.e. now

async function runBuyPosition() {
    // if current position is 0, post a buy (first DCA  level)
    let position = orderBookData.getpositionSize();
    let currentPrice = orderBookData.getAsk();
    if (bias === "Buy") {
        currentPrice = orderBookData.getBid();
    }
    let avgEntryPrice = orderBookData.getavgEntryPrice();
    if (userConfig.symbol === "XBTUSD") {
        avgEntryPrice = Math.floor(avgEntryPrice);
    } else {
        avgEntryPrice = round(avgEntryPrice, 1);
    }
    let currentDCALevel = determineCurrentDCALevel(position);

    if (isNaN(avgEntryPrice)) {
        avgEntryPrice = 0;
    }
    if (originalavgEntryPrice === 0) {
        originalavgEntryPrice = avgEntryPrice;
    }
    if (store.get('stopTrading') !== "true") {
        if (position === 0 && store.get('currentState') !== "BUYING" && canWeEnterNewTrade()) {
            // initial buy...
            store.set('currentState', "BUYING");
            store.set('sellOrderPlaced', 'false');
            lastDCATime = moment().tz('Europe/Amsterdam').format(); // reset
            DCAOrdersCreated = false;
            await order.cancelAllOrders(symbol);
            // oude manier van postOrder
            const result = await postOrder(bias);
            // nieuwe manier met participateDoNotInitiate

            // TODO: result can return an error and then the order is not filled??....
            if (result) {
                try {
                    // TODO: deze komt terug als:
                    // 0|bitmex-o | 2020-03-20 01:01 +08:00: cannot read ordStatus of undefined
                    // 0|bitmex-o | 2020-03-20 01:01 +08:00: error:{}
                    // 0|bitmex-o | 2020-03-20 01:01 +08:00: result:undefined

                    if (result.ordStatus) {
                        if (result.ordStatus == "New") {
                            order.getLimitOrderFilled(result);
                        } else {
                            console.log("ISSUE: result.ordStatus= " + JSON.stringify(result.ordStatus));
                        }
                    }
                } catch (e) {
                    console.log("cannot read ordStatus of undefined");
                    console.log("error:" + JSON.stringify(e))
                    console.log("result:" + JSON.stringify(result));
                }
            } else {
                console.log("ISSUE: result is: " + JSON.stringify(result));
            }
        } else if (position === 0 && store.get('currentState') !== "BUYING" && store.get('currentState') !== "FLAT" && !canWeEnterNewTrade()) {
            await order.cancelAllOrders(symbol);
        }
    } else {
        console.log("stoptrading is true");
    }
    // determine 80% of the minimumOrderSize...
    const minimumPositionsize80percent = getMinimumPositionSize() * 0.8;
    if (position !== 0 && (store.get('sellOrderPlaced') !== 'true')) {
        //we've taken a position but the sell order is not yet placed..hence a new position!
        if (position > minimumPositionsize80percent) {

        }
        console.log("setting originalavgEntryPrice:" + avgEntryPrice);
        originalavgEntryPrice = avgEntryPrice;
    }
    if ((position !== 0 && (store.get('sellOrderPlaced') !== 'true')) || (avgEntryPrice !== lastKnownavgEntryPrice && position !== 0)) {
        if (position > minimumPositionsize80percent) {
            lastKnownavgEntryPrice = avgEntryPrice;
            lastDCATime = moment().tz('Europe/Amsterdam').format(); // i.e. now
            store.set('currentState', "BOUGHT");
            store.set('sellOrderPlaced', 'true');
            placeSellOrders(position, originalavgEntryPrice, avgEntryPrice, currentPrice, currentDCALevel);
        }
    }
}


function round(value, precision) {
    var multiplier = Math.pow(10, precision || 0);
    return Math.round(value * multiplier) / multiplier;
}

let ethSettingWarning = false
function getMinimumPositionSize() {
    // Bitmex demands a minimum quantity  of 0.01 bitcoin and round it down to 10...
    // console.log("minOrderSize = " + Math.ceil(((orderBookData.getBid() * 0.01)+1)/10)*10);
    if (!ethSettingWarning) {
        ethSettingWarning = true;
        console.log("WARNING, THIS SETTING DOES NOT WORK WHEN TRADING ETH")
    }
    return minimumOrderSize = Math.ceil(((orderBookData.getBid() * 0.01) + 1) / 10) * 10;
    // return 5;
}

async function runDCACheck(overridePlaceOrders) {
    // determine current position:
    if (typeof overridePlaceOrders === 'undefined') {
        overridePlaceOrders = false;
    }

    let position = orderBookData.getpositionSize();
    // console.log("DEBUG: positionsize during DCA check:" + position);
    // TODO for the DCAcheck implement a RESTful call because somehow it's keeping the wrong value?
    let currentPrice = orderBookData.getAsk();
    let avgEntryPrice = Math.floor(orderBookData.getavgEntryPrice());
    if (isNaN(avgEntryPrice)) {
        avgEntryPrice = 0;
    }
    if (originalavgEntryPrice === 0) {
        originalavgEntryPrice = avgEntryPrice;
    }
    // determine current DCA level
    let currentDCALevel = determineCurrentDCALevel(Math.abs(position));
    let DCAParametersForLog = "position: " + position + ", avgEntryPrice: " + avgEntryPrice + ", originalAvgTry: " + originalavgEntryPrice + " currentDCALevel: " + currentDCALevel;
    if (DCAParametersForLog !== DCAParameters) {
        logger.info(DCAParametersForLog);
        DCAParameters = DCAParametersForLog;
    }
    let closeOrderFound = false;
    let DCAOrderFound = false;
    const orders = await order.getOrdersByPOST();
    if (orders.length > 0) {
        for (let i = 0; i < orders.length; i++) {
            if (orders[i].side === ((position > 0) ? "Buy" : "Sell")) {
                DCAOrderFound = true;
            }
        }
    }
    if (overridePlaceOrders) {
        // cancell all orders
        order.cancelAllOrders(symbol).then(function (res) {
            store.set('sellOrderPlaced', 'false');
            placeDCAAndSellOrders_v2(position, originalavgEntryPrice, avgEntryPrice, currentPrice, currentDCALevel);
        });
    } else if (!DCAOrderFound) {
        placeDCAAndSellOrders_v2(position, originalavgEntryPrice, avgEntryPrice, currentPrice, currentDCALevel);
    }
}

async function runStrategyMonitor() {
    // console.log("running strategy monitor");
    // validate if the position size is correct...
    let position = orderBookData.getpositionSize();
    const emailSent = store.get('emailSent');
    // if (position < -250 || position > 250) {
    //     //send an email if it hasnt been sent yet.
    //     if (emailSent === 'false') {
    //         mailOptions.html = 'position is negative or position is greater than 250, please have a look ASAP.';
    //         store.set('emailSent', 'true');
    //         transporter.sendMail(mailOptions, function (err, info) {
    //             if (err)
    //                 console.log(err)
    //             else
    //                 console.log(info);
    //         });
    //     }
    // }
    if (position > 0 || position < 0) {
        const orders = await order.getOrdersByPOST();
        if (orders.length > 0) {
            // check if there is at least one sell order..
            let closeOrderFound = false;
            const closeOrderSide = (position > 0 ? "Sell" : "Buy");
            for (let i = 0; i < orders.length; i++) {
                if (orders[i].side === closeOrderSide) {
                    closeOrderFound = true;
                }
            }
            if (!closeOrderFound) {
                //TBD: cancel all and market out?
                mailOptions.html = 'monitor just noticed that there is no close order but there is a position, please have a look ASAP.';
                console.log('monitor just noticed that there is no sell order but there is a position');
                store.set('sellOrderPlaced', 'false');
                // transporter.sendMail(mailOptions, function (err, info) {
                //     if (err)
                //         console.log(err)
                //     else
                //         console.log(info);
                // });
            }
        } else {
            // no orders found but there is a position...
            await wait(5000);
            // check again if there are no orders...
            const orders = await order.getOrdersByPOST();
            if (orders.length > 0) {
                // order found, no issues. Could also be a DCA though...
            } else {
                // still no order found..
                console.log("strat monitor: Still no orders found. ")
                store.set('sellOrderPlaced', 'false');
            }
        }
    }
    let currentState = store.get('currentState');
    if (position === 0 && (currentState === "BUYING" || currentState === 'InPOSITION')) {
        // check if there is a buy order?
        const orders = await order.getOrdersByPOST();
        if (orders.length === 0) {
            // something went wrong....update the currentState back to 'flat'.
            await wait(5000);
            //is the status still buying?
            currentState = store.get('currentState');
            if (currentState === "BUYING" || currentState === 'InPOSITION') {
                // update the status to FLAT
                console.log("monitor is setting currentState to FLAT");
                store.set('currentState', 'FLAT');
                mailOptions.html = 'monitor just put currentState to FLAT, please have a look ASAP.';
                // transporter.sendMail(mailOptions, function (err, info) {
                //     if (err)
                //         console.log(err)
                //     else
                //         console.log(info);
                // });
            }
        }
    }
}

async function placeDCAAndSellOrders_v2(position, originalavgEntryPrice, avgEntryPrice, currentPrice, currentDCALevel) {
    //determine the size per SellLevel.
    if (position === 0) {
        // exit because if position is 0 then we should not DCA, there's nothing to DCA?
        return;
    }
    let currentPosition = "Sell";
    if (position > 0) {
        currentPosition = "Buy";
    }
    // looking at the las time we DCAd, can we DCA again?
    if (lastDCATime > moment().subtract(40, 'seconds').tz('Europe/Amsterdam').format()) {
        // it is not five minutes ago since the last time we DCA'd

        //console.log("skipping a DCA run since lastDCATime is less than 5 minutes");
    } else {
        lastDCATime = moment().tz('Europe/Amsterdam').format(); // reset
        // set a DCA...
        // two things to determine.
        // 1. the size ->
        // 1a. what the current DCA level is?
        // 2. the location? -> right where we are?
        // 3. check if there is not alreadypar a DCA order present?
        const minPositionSize = getMinimumPositionSize();
        let DCASize = determineDCASizePerLevel(minPositionSize)[currentDCALevel];
        const DCAPriceLevels = determineDCALevels(originalavgEntryPrice, currentPrice, currentDCALevel, currentPosition, true)[currentDCALevel];
        if (DCAPriceLevels === 0) {
            //do not DCA...
            return;
        } else {
            const side = (currentPosition === "Sell" ? "Sell" : "Buy");
            let price = 0;
            if (side === 'Buy' && currentPrice < DCAPriceLevels) {
                price = currentPrice - 2;
            } else if (side === 'Sell' && currentPrice > DCAPriceLevels) {
                price = currentPrice + 2;
            } else {
                price = DCAPriceLevels;
            }
            let orders = [];
            // todo (nice to have); spread these orders over different locations?
            if (DCASize > 20001) {
                DCASize = 20000;
            }
            orders.push({
                symbol: symbol,
                orderQty: DCASize,
                ordType: 'Limit',
                price: price,
                side: side,
                text: 'DCA',
                execInst: 'ParticipateDoNotInitiate'
            });
            order.postBulkOrderSimple(orders);
        }
    }
}

function placeSellOrders(position, originalavgEntryPrice, avgEntryPrice, currentPrice) {
    let side = "Buy";
    if (position > 0) {
        // We're having a buy position so create sell orders...
        side = "Sell";
    }
    let bias = store.get('5mtrend');
    // BUG: bij het opnieuw plaatsen van de buy orders...pakt ie de NIEUWE avgEntryPrice waardoor meteen een nieuwe DCA level 1 order wordt gekocht. Dit aanpassen naar fixed DCA levels?
    order.cancelAllOrders(symbol).then(function (res) {
        if (bias !== "Buy") {
            // we should get rid of this position ASAP.
            // new feature: try to sell at 0,07% higher or lower to get rid of the position asap. The maker rebate is 0,07%.
            // let newAvgentryPrice = 0;
            if (position > 0) {
            //     //create a sell order.
            //     console.log("setting sell order at 0.0007")
            //     newAvgentryPrice = Math.round(avgEntryPrice / (1 + 0.0007));
                if (currentPrice > newAvgentryPrice) {
                    newAvgentryPrice = currentPrice;
                }
            } else {
            //     //create a buy order.
            //     newAvgentryPrice = Math.round(avgEntryPrice * (1 + 0.0007));
                if (currentPrice < newAvgentryPrice) {
                    newAvgentryPrice = currentPrice;
                }
            }
            order.postOrderSimple(side, "Limit", avgEntryPrice, "selling at avgEntry", Math.abs(position))
         } else {
            //happy in this position..let's sell it for some profit.
            let sellLevels = determineSizePerSellLevel(position, 5);
            // if the position is the first, try to sell it a bit higher than usual.
            if (position === getMinimumPositionSize()) {
                console.log("setting sell order at 0.0005")
                avgEntryPrice = Math.round(avgEntryPrice * (1 + 0.0001));
            }
            console.log("happy in this position..let's sell it for some profit.");
            //sell orders use the CURRENT average entry price because you always want to sell close to the actual avg entry price
            contSellLevelsInput = (side === "Buy" ? "Sell" : "Buy");
            const sellOrders = createOrders(side, determineSellLevels(avgEntryPrice, 5, currentPrice, contSellLevelsInput), sellLevels, "selling at avgEntry+1", currentPrice);
            // TODO: determine sizes per DCA level...
            if (sellOrders.length > 0) {
                order.postBulkOrderSimple(sellOrders);
            } else {
                console.log("WARNING: placeSellOrders is creating 0 orders...");
                console.log("WARNING INFO: " + avgEntryPrice + " " + currentPrice + " " + contSellLevelsInput + " " + JSON.stringify(sellLevels));
            }
        }
    });
}

function determineCurrentDCALevel(position) {
    let DCALevels = 1;
    const minPositionSize = getMinimumPositionSize();
    if (position < minPositionSize) {
        return 0;
    }
    if (position === 0) {
        return 0;
    }
    const x = Math.round((position / minPositionSize));
    switch (true) {
        case (x < 2):
            DCALevels = 0;
            break;
        case (x < 5):
            DCALevels = 1;
            break;
        case (x < 14):
            DCALevels = 2;
            break;
        case (x < 41):
            DCALevels = 3;
            break;
        // case (x > 32):
        //     DCALevels = 4;
        //     break;
        // case (x < 64):
        //     DCALevels = 5;
        //     break;
        // case (x < 128):
        //     DCALevels = 6;
        //     break;
        // case (x < 178):
        //     DCALevels = 7;
        //     break;
        default:
            DCALevels = 4;
            break;
    }
    return DCALevels;
}

function determineSizePerSellLevel(currentPositionSize, sellLevels) {
    currentPositionSize = Math.abs(currentPositionSize);
    roundedDown = Math.floor(currentPositionSize / sellLevels);
    let minPositionSize = getMinimumPositionSize();
    if (roundedDown < minPositionSize) {
        roundedDown = minPositionSize;
    }
    let leftOverPosition = currentPositionSize;
    const sizes = [];
    for (let i = 0; i < sellLevels; i++) {
        if (i == (sellLevels - 1)) {
            if (leftOverPosition !== 0) {
                let lastSize = sizes[sizes.length - 1];
                lastSize = lastSize + leftOverPosition;
                sizes.pop();
                sizes.push(lastSize);
                // sizes.push(leftOverPosition);
            }
        } else {
            if (leftOverPosition === 0) {
                //skip
            } else {
                if (roundedDown > currentPositionSize) {
                    sizes.push(currentPositionSize);
                    leftOverPosition = leftOverPosition - currentPositionSize;
                } else if (leftOverPosition < roundedDown) {

                    // get size and add this to the last order...
                    let lastSize = sizes[sizes.length - 1];
                    lastSize = lastSize + leftOverPosition;
                    sizes.pop();
                    sizes.push(lastSize);
                    // sizes.push(leftOverPosition);
                    leftOverPosition = leftOverPosition - leftOverPosition;
                } else {
                    sizes.push(roundedDown);
                    leftOverPosition = leftOverPosition - roundedDown;
                }
            }
        }

    }
    return sizes;
}

function determineDCASizePerLevel(initialSize) {
    const DCASizes = [];
    // initial size times two because that is the real first DCA level (if you bu
    var size = initialSize;
    for (let i = 0; i < userConfig.DCALevels.length; i++) {
        if (i === 0) {
            DCASizes.push(initialSize * 3);
            size = 3 * size;
        } else {
            DCASizes.push(3 * size);
            size = 3 * size;
        }
    }
    return DCASizes;
}

function createOrders(side, prices, size, annotationText, currentPrice) {
    const orders = [];
    for (let i = 0; i < prices.length; i++) {
        if ((side === 'Buy' && currentPrice > prices[i]) || (side === 'Sell' && currentPrice < prices[i])) {
            if (!size[i] || prices[i] === 0) {
                //skip order because there's no size thus no valid order
            } else {
                orders.push({
                    symbol: symbol,
                    orderQty: size[i],
                    ordType: 'Limit',
                    price: prices[i],
                    side: side,
                    text: annotationText,
                    execInst: 'ParticipateDoNotInitiate'
                })
            }
        }
    }
    return orders;
}

// async function createandPostOrders(prices) {
//     for (let i = 0; i < prices.length; i++) {
//         await wait(3000);
//         order.postOrderSimple("Buy", "Limit", Math.round(prices[i]), "DCA level " + i, 25);
//     }
// }

function wait(milliseconds) {
    return new Promise(resolve => setTimeout(resolve, milliseconds));
}

function determineDCALevels(avgEntryPrice, currentPrice, currentDCALevel, currentSide, overridePriceZero) {
    const prices = [];
    for (let i = 0; i < userConfig.DCALevels.length; i++) {
        if (!userConfig.DCALevels[i]) {
            // there are no more DCALevels left...
            return 0;
        }
        let newPrice = 0
        if (currentSide === "Buy") {
            newPrice = Math.round(avgEntryPrice * (1 - (userConfig.DCALevels[i] / 100)));
            if (currentPrice < newPrice || newPrice === currentPrice || currentDCALevel > (i + 1)) {
                //skip this one..
                if (!overridePriceZero) {
                    console.log("skipping DCA level " + userConfig.DCALevels[i] + " currentDCALevel: " + currentDCALevel + " i: " + i - 1);
                    prices.push(0);
                } else {
                    prices.push(newPrice);
                }

            } else {
                prices.push(newPrice);
            }
        } else {
            newPrice = Math.round(avgEntryPrice * (1 + (userConfig.DCALevels[i] / 100)));
            if (currentPrice > newPrice || newPrice === currentPrice || currentDCALevel > (i + 1)) {
                //skip this one..
                if (!overridePriceZero) {
                    console.log("skipping DCA level " + userConfig.DCALevels[i] + " currentDCALevel: " + currentDCALevel + " i: " + i - 1);
                    prices.push(0);
                } else {
                    prices.push(newPrice);
                }
            } else {
                prices.push(newPrice);
            }
        }
    }
    return prices;
}

function determineSellLevels(avgEntryPrice, sellLevels, currentPrice, side) {
    const prices = [];
    if (side === "Buy") {
        // when the side is 'buy', then we're creating sell orders to close the buy position and vice versa
        if (avgEntryPrice < currentPrice) {
            avgEntryPrice = currentPrice + 1;
        }
        // for now just splitting in 5 levels...
        for (let i = 0; i < sellLevels; i++) {
            let newPrice = Math.round((avgEntryPrice * (1 + ((0.05 * i) / 100))));
            prices.push(newPrice);
        }
    } else {
        if (avgEntryPrice > currentPrice || avgEntryPrice === currentPrice) {
            avgEntryPrice = currentPrice - 1;
        }
        // for now just splitting in 5 levels...
        for (let i = 0; i < sellLevels; i++) {
            let newPrice = Math.round((avgEntryPrice / (1 + ((0.05 * i) / 100))));
            prices.push(newPrice);
        }
    }
    console.log(JSON.stringify(prices));
    return prices;
}

// side : "Buy" or "Sell"
async function postOrder(side) {
    try {
        var result = await order.postOrder(side, 1, "Limit", 0, "going to " + side);
        return result;
    } catch (e) {
        console.log(JSON.stringify(e));
        return 1;
    }
}

async function Init() {
    if (!userConfig.unitTestMode) {
        console.log("Running init function");
        console.log("Trading enable:" + userConfig.tradingEnabled);
        orderBookData.setpositionSize(0);
        orderBookData.setorderCooldownActive(false);
        console.log("Setting cooldown to false");
        store.set('emailSent', 'false');
        console.log("setting email sent to false");
        // monitor price
        monitorOrderbook();

        // determine if we should DCA or not...

        await Timeout.set(3000);
        monitorPosition();
        await Timeout.set(2000);
        initTrading();
    } else {
        console.log("unitTestMode is enabled...")
    }
}


function convertCandles(one_m_candlesBucketed, convertTom15) {
    var close = [];
    var volume = [];
    var volumeAdded = 0;
    var count = 0;
    // if (userConfig.convertm5CandlesTom15) {
    //     if (userConfig.binSize != "5m") {
    //         logger.error("CONFIG ERROR: conver 5m candles is set but binSize is not 5m");
    //     }
    // }
    if (convertTom15) {
        let currentMinute = one_m_candlesBucketed[0].timestamp.substring(14, 16);
        if (currentMinute > 0 && currentMinute < 10 ||
            currentMinute > 15 && currentMinute < 25 ||
            currentMinute > 30 && currentMinute < 40 ||
            currentMinute > 45 && currentMinute < 55) {
            //pop one candle
            one_m_candlesBucketed.shift();
        } else if (currentMinute > 9 && currentMinute < 15 ||
            currentMinute >= 20 && currentMinute < 30 ||
            currentMinute >= 40 && currentMinute < 45 ||
            currentMinute >= 50 && currentMinute <= 59) {
            //pop two candles
            one_m_candlesBucketed.shift();
            one_m_candlesBucketed.shift();
        }
        let countToThree = 3;

        one_m_candlesBucketed.forEach(function (value) {
            if (countToThree === 3) {
                close.push(value.close)
            }
            volumeAdded = volumeAdded + value.volume;
            countToThree--;
            if (countToThree == 0) {
                countToThree = 3;
                volume.push(volumeAdded);
                volumeAdded = 0;
            }
        });
    } else {
        one_m_candlesBucketed.forEach(function (value) {
            count++;
            if (count > 100) {
                return true;
            } else {
                close.push(value.close);
                volume.push(value.volume);
            }
        });
    }
    let returnObj = {};
    returnObj.close = close;
    returnObj.volume = volume;
    return returnObj;
}

async function determineTradingParameters() {
    // get h1 bias
    const h1bias = await determineBuyOrSellBias('1h', false);
    // get m15 bias
    const m15bias = await determineBuyOrSellBias('5m', true);
    // // get m5 bias
    const m5bias = await determineBuyOrSellBias('5m', false);
    // // log the levels..
    console.log('current bias settings: h1: ' + JSON.stringify(h1bias) + ' m5:  ' + JSON.stringify(m5bias));
    //(h1ema21, h1ema7, h1rsi, m5rsi)
    calculateLongcondition(h1bias.ema21, h1bias.ema7, h1bias.rsi, m5bias.rsi);
    // TODO: reset the current orders?
}

async function determineBuyOrSellBias(binSize, convertTom15) {
    let _bias = 'Buy';
    const one_m_candlesBucketed = await order.getCandles(binSize);
    if (typeof one_m_candlesBucketed === "undefined") {
        logger.info("candles are undefined...")
        return 0;
    } else {
        var close = [];
        var volume = [];
        const tradeData = convertCandles(one_m_candlesBucketed, convertTom15)
        close = tradeData.close;
        volume = tradeData.volume;
        //console.log("BEFORE BRINGING THIS VERSION OUT, UPDATE THE CONFIG.JS convertm5CandlesTom15.")

        // TODO; do something with this in the future...?
        // //scam pump protection...be sure to send this before the close.reverse stuff because else you'll look at the wrong candles.
        // var scamWickDetected = await scamwickDetection(one_m_candlesBucketed, 24);

        //remove the last one since that is the close of the new candle...
        //var newclose = close.pop();
        //WARNING, THIS DOES NOT WORK....closeNewtoOld will always get the same array as close...use close[20] for the last candle bar close
        //closeNewToOld = close;
        //data for tulind should be from ""Data order is from oldest to newset (index 0 is the oldest)""
        var closeReverse = close.reverse();

        //note to self, before debugging this, check what the testnet setting is set to.
        var rsiCalculated = await tulind.indicators.rsi.indicator([closeReverse], [14]);
        var tulipemacalced = await tulind.indicators.ema.indicator([closeReverse], [21]);
        var tulipemacalced7 = await tulind.indicators.ema.indicator([closeReverse], [7]);
        var volumeEMA7 = await tulind.indicators.ema.indicator([volume], [7]);
        var volumeEMA21 = await tulind.indicators.ema.indicator([volume], [21]);
        //var volumeSMA7 = await tulind.indicators.ema.indicator([close], [7]);
        //var ema21 = tulipemacalced[0][99];
        //var ema7 = tulipemacalced7[0][99];
        var ema21 = tulipemacalced[0][tulipemacalced[0].length - 1]
        var ema7 = tulipemacalced7[0][tulipemacalced7[0].length - 1]
        var rsi = rsiCalculated[0][rsiCalculated[0].length - 1];
        var volumeOscilated = (((volumeEMA7[0][volumeEMA7[0].length - 1] - volumeEMA21[0][volumeEMA21[0].length - 1]) / volumeEMA21[0][volumeEMA21[0].length - 1]) * 100)

        if (ema21 < ema7) {
            _bias = "Buy";
        } else {
            _bias = "Sell";
        }
        binSize = convertTom15 === true ? "15m" : binSize;
        store.set(binSize + 'trend', _bias);
        return {
            ema21: round(ema21,1),
            ema7: round(ema7,1),
            rsi: round(rsi,1),
            bias: _bias
        };
    }
}

function canWeEnterNewTrade() {
    // get the longConditionTimeStamp
    const longCondition = store.get('longConditionTimeStamp');
    // console.log("longCondition" + longCondition);
    // if (longCondition) {
    //     if (longCondition > moment().subtract(5, 'minutes').tz('Europe/Amsterdam').format()) {
    //         console.log("yes we can enter new trade " + moment().subtract(60, 'minutes').tz('Europe/Amsterdam').format());
            return true;
    //     }
    // }
    return false;
}

function calculateLongcondition(h1ema21, h1ema7, h1rsi, m5rsi) {
    // console.log("ema7: " + ema7 + " ema 21: " + ema21 + " rsi: " + rsi);
    if (((h1ema7 > h1ema21) && h1rsi < 50) || (h1rsi < 34 && m5rsi < 80)) {
        //if (rsi < 60) {
        // long condition...
        console.log("setting long condition to: " + moment().tz('Europe/Amsterdam').format());
        store.set('longConditionTimeStamp', moment().tz('Europe/Amsterdam').format());
    }
}

// determineTradingParameters();
// if (store.get('stopTrading') !== 'true') {
Init();
// determineTradingParameters();

// store.set('longConditionTimeStamp', moment().tz('Europe/Amsterdam').format());
// canWeEnterNewTrade();
// // //}
