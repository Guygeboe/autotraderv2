const fs = require('fs');
const path = require('path');
const env = process.env.NODE_ENV || 'development';
const logDir = 'log';
const { createLogger, format, transports } = require('winston');
const winston = require('winston');
const moment = require('moment-timezone');
const appendTimestamp = format((info, opts) => {
  if(opts.tz)
    info.timestamp = moment().tz(opts.tz).format();
  return info;
});
const { LEVEL, MESSAGE } = require('triple-beam');

//const myFormat = printf(info => `${info.timestamp} [${info.level}]: ${info.label} - ${info.message}`);

require('winston-daily-rotate-file');
// Create the log directory if it does not exist
if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
  }
  
  const dailyRotateFileTransport = new transports.DailyRotateFile({
    filename: `${logDir}/%DATE%-results.log`,
    datePattern: 'YYYY-MM-DD',
    level: 'info',
    json: true
  });
  
  const logger = createLogger({
    // change level if in dev environment versus production
    level: env === 'verbose', //'development' ? 'verbose' : 'info',
    format: format.combine(
      format.timestamp({
        format: 'YYYY-MM-DD HH:mm:ss'
      }),
      appendTimestamp({ tz: 'Europe/Amsterdam' }),
      format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
    ),
    transports: [
      // new transports.Console({
      //   level: 'info',
      //   format: format.combine(
      //     format.colorize(),
      //     format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`
      //     )
      //   )
      // }),
      new winston.transports.Console({
        //
        // Possible to override the log method of the
        // internal transports of winston@3.0.0.
        //
        log(info, callback) {
          setImmediate(() => this.emit('logged', info));
  
          if (this.stderrLevels[info[LEVEL]]) {
            console.error(info[MESSAGE]);
        
            if (callback) {
              callback();
            }
            return;
          }
        
          console.log(info[MESSAGE]);
        
          if (callback) {
            callback();
          }
        }
      }),
      dailyRotateFileTransport
    ]
  });

module.exports = logger;