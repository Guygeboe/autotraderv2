//old implementation
//const fetch = require('fetch-retry');
//new implementation
//const fetch2 = require('node-fetch-retry');
const fetch = require('node-fetch');
const userConfig = require('./config/config');

var crypto = require('crypto');
const logger = require('./logger');
const index = require('./index');
var Timeout = require('await-timeout');


const qs = require('qs');

if (userConfig.testnet) {
  _apiKey = index.apiKeyInfo.TEST_apiKey;
  _apiSecret = index.apiKeyInfo.TEST_apiSecret;
}
else {
  _apiKey = index.apiKeyInfo._apiKey;
  _apiSecret = index.apiKeyInfo._apiSecret;
}

var _url = "";
if (userConfig.testnet) {
  _url = "https://testnet.bitmex.com";
}
else {
  _url = "https://www.bitmex.com";
  logger.info("!!! WARNING, TESTNET SETTING IS SET TO FALSE (" + userConfig.testnet + ")");
}

async function makeRequestCallback(verb, endpoint, data, callback) {
  var result = await makeRequest(verb, endpoint, data);
  callback(result);
}

async function makeRequest(verb, endpoint, data = {}, contentType) {
  const apiRoot = '/api/v1/';
  let _contentType = 'application/json';
  // if (contentType === 'x-www-form-urlencoded') {
  //   _contentType = contentType
  // }
  let query = '', postBody = '';
  if (verb === 'GET' || ( verb === 'DELETE' && contentType === 'application/json'))
    query = '?' + qs.stringify(data);
  else if (contentType === 'x-www-form-urlencoded') {
    // postBody = 'orderID=' + data;
    //postBody =  encodeURIComponent('orderID') + '=' + encodeURIComponent(data);
    // postBody = qs.stringify({'orderID' : data});
    postBody = qs.stringify(data);
    _contentType = contentType;
  }
  else {
    // Pre-compute the reqBody so we can be sure that we're using *exactly* the same body in the request
    // and in the signature. If you don't do this, you might get differently-sorted keys and blow the signature.
    postBody = JSON.stringify(data);
  }
  // console.log("POSTBODY: " + postBody);

  const expires = Math.round(new Date().getTime() / 1000) + 60; // 1 min in the future
  const signature = crypto.createHmac('sha256', _apiSecret)
      .update(verb + apiRoot + endpoint + query + expires + postBody)
      .digest('hex');

  const headers = {
    'content-type': _contentType,
    'accept': 'application/json',
    // This example uses the 'expires' scheme. You can also use the 'nonce' scheme. See
    // https://www.bitmex.com/app/apiKeysUsage for more details.
    'api-expires': expires,
    'api-key': _apiKey,
    'api-signature': signature,
  };
  const requestOptions = {
    method: verb,
    headers,
  };
  if (verb !== 'GET') requestOptions.body = postBody;  // GET/HEAD requests can't have body
  const url = _url + apiRoot + endpoint + query;
  requestOptions.retry = 250;
  let res = await fetch_retry_custom(url, requestOptions);

  if (endpoint === "order" && verb !=="GET") {
    // logger.info("bitmexrequestOrder: " + JSON.stringify(res));
  }
  if (endpoint === "order/all") {
    logger.info("cancel all orders was: " + res.status + res.statusText);
  }
  return res;
  //}
};
// return fetch(url, requestOptions
// ).then(function (response) {
// return response.json();
// }).then(function (json) {
// // do something with the result
// if (json.message){
//   if(json.message === "The system is currently overloaded. Please try again later.")
//   {
//     //retry the request...
//     logger.info("overload caught...possibly create something around this...")

//   }
// }
// // var err = new Error('example')
// // throw err
// return json;
// }).catch(function (error) {
// console.error('Network error', error);
// logger.error('error ' + error);
//return error;

async function fetch_retry_custom(url, opts) {
  let retry = opts && opts.retry || 5;
  while (retry > 0) {
    try {
      var res = await fetch(url, opts);
      var result = await res.json();
      result.status = res.status;
      result.statusText = res.statusText;
      result.headers = res.headers;
      if(parseInt(result.headers.get('x-ratelimit-remaining')) < 15){
        logger.info("reaching rate limit limit: " + result.headers.get('x-ratelimit-remaining'));
        await Timeout.set(2500);
      }
      if (typeof result.error !== 'undefined') {
        //error occured, go flat?

        logger.error("error occured: " + JSON.stringify(result.error));
        if (retry == 0) {
          //out of retries but we got an error...return this to go flat.
          return result;
        }
        else if(result.error.message === "Invalid ordStatus")
        {
          return result;
        }
        else if(result.error.message === "Invalid orderQty")
        {
          console.log("adding something here to be able to debug this...");
          return result;
        }
        else if (result.error.message.indexOf('overload') !== -1 ) {
          console.log("debug: overload occurring...");
          //retry this request...
          //TODO make this a global variable...
          logger.info("current rate limit limit: " + result.headers.get('x-ratelimit-remaining'));
          if (parseInt(result.headers.get('x-ratelimit-remaining')) < 5) {
            //reaching ratelimit limit, wait two seconds with request retry
            logger.info("reaching rate limit limit: " + result.headers.get('x-ratelimit-remaining'));
            await Timeout.set(2500);
          }
          throw Error(result.error.message);
        }
        else {
          return result;
        }
        //throw Error(result.error);
      }
      else {
        return result;
      }
    } catch (e) {
      logger.error("network error:, retrying (attempt " + retry + "): " + e);
      await Timeout.set(100);
      retry = retry - 1
      if (retry == 0) {
        throw e
      }
    }
  }
};


module.exports = {
  makeRequest: makeRequest,
  makeRequestCallback: makeRequestCallback
}
