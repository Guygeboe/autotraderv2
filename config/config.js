const userConfig = {
    'unitTestMode' : false,
    'tradingEnabled' : true,
    'stoplossEnabled' : false,
    'DCALevels' : [
        0.3, 1.2, 5, 9, 30  //, 5.4, //, 7.4
    ],
    'symbol': 'XBTUSD',
    'positionSize': 270, // this value has become obsolete...
    'maxTableLen' : 100000,
    'dynamicPosition' : false,
    'verifyPostedOrder' : false,
    'volumeOscilatedMinValue' : 0, //should be 0
    'testnet' : false,
    'paperTrading' : false,
    'binSize' : '1h', //valid values are 1m, 5m, 1h
    'convertm5CandlesTom15' : false,
    'sellLevel1' : 0.001, //0.008 = 0.8% profit.
    'stoplossPercentage' : -0.65,
    'stoplossPercentageAfterSellSignal' : 0.07, //this stoploss percentage will be used
    'websocketServer' : false,
    'marketOrderOnly' : false,
    'TrailInsteadOfLimitIfTouched' : true,
    'maxTrailPercentageFromBottom' : 1.003, //1.017  is 1.7% for 1h and let's set it to 0.4% (i.e. 1.004) on 5m
    'tradeOnCloseOnly' : true,
    'piercingEMADelay' : true,
    'piercingEMADelayTimeMilliseconds' : 50000,
    'piercingEMADelayTimeMillisecondsExpiration' : 6000,
    'maxScamwickPercentage': 1, //1 is 1%
    'mail': {
      'username': 'guyalerts1@gmail.com',
      'password': 'QwY7phqbTQnQj77',
      'host': 'imap.gmail.com',
      'port': 993,
      'mailbox': 'INBOX',
      'maxMailAge': 60
    },
    'retry': {
      'retries': 30,
      'minTimeout': 200,
      'maxTimeout': 6000,
    },
    'tradingview': {
      //'mail': 'noreply@tradingview.com'
      'mail': 'guyhagemans@gmail.com'
    }

  }

  module.exports = userConfig;
