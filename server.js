var express = require('express')
var app = express()
var botInfo = require('./index').botInfo;

app.get('/', function (req, res) {
   res.sendfile(__dirname + '/ws.html');
})
app.listen(3000, function () {
   console.log('Example app listening on port 3000!')
})

const WebSocket = require('ws')
const wss = new WebSocket.Server({ port: 40510 });

wss.on('connection', function(ws) {
    const id = setInterval(function() {
      ws.send(JSON.stringify(botInfo), function() {
        //
        // Ignore errors.
        //
      });
    }, 100);
    console.log('started client interval');
  
    ws.on('close', function() {
      console.log('stopping client interval');
      clearInterval(id);
    });
  });